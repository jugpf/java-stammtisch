# Pforzheimer Java Stammtisch

Einmal im Quartal veranstaltet die *Java User Group Goldstadt* den **Pforzheimer
Java Stammtisch**, zu welchem alle an Java (der Sprache, der Plattform, dem Öko-
System) Interessierten herzlich eingeladen sind.

Zur Teilname an einem Treffen bitten wir um vorherige Anmeldung per EMail an anmeldung-jugpf@gmx.de . Die aktuellen Corona-Bestimmungen sind zu beachten.

## Nächstes Treffen

  **28.12.2023 19:00 Online-Vortrag

  * *Thema* - **Hacking OpenJDK**: Hurra, ich habe Java schneller gemacht!
  * *Referent* - Markus Karg
  * *Teilnahme* - Über Element Call (läuft im Browser). Teilnahme ist kostenlos (auch für Nichtmitglieder)
  * *Anmeldung* - Ein Link zum Call wird auf Anfrage an anmeldung-jugpf@gmx.de versendet.

## Zukünftige Treffen

  * 28.12.2023

## Bisherige Treffen
  * 30.08.2023 19:00
  "Biergarten Seehaus Pforzheim"** (Tiefenbronner Straße 201, 75175 Pforzheim) - https://www.seehauspforzheim.de (@gzilly)

    * *Neuigkeiten* - Allgemeine Neuigkeiten, anstehende Veranstaltungen und Konferenzen
    * *Java21* - Infos zum neuen Release.
    * *Sonstiges*

  * 28.01.2021 19:00 (Online)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Planungen* - Teilnahme Javaland 2021
    * *Java-Flipper* - Vorstellung des Java-Flipper, den wir im Rahmen der Veranstaltung Java4Kids entwickelt haben.
    * *Sonstiges*

  * 30.01.2020 19:00
   "Im Schlupf" (Heidenheimer Straße 7-9, 75179 Pforzheim) - www.im-schlupf.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Planungen* - Teilnahme Javaland 2020
    * *Jug Homepage* - aktueller Stand
    * *Sonstiges und Termine*

  * 21.11.2019 19:00
   "Zum Faulenzer" (Bürgerstr. 4, Pforzheim-Brötzingen) - http://zumfaulenzer.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Planungen* - Teilnahme Javaland 2020 / JuG Ausflug
    * *Sonstiges und Termine*

  * 26.09.2019 19:00
    "Chez Ernschtle" (Kreuzstr. 17, Pforzheim) - https://www.facebook.com/ChezErnstle (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *EclipseConf* - Teilnahme EclipseCon Europe 2019 in Ludwigsburg
    * *Javaland 2020* - Planung für das Javaland 2020
    * *Sonstiges und Termine*

  * 25.07.2019 19:00
    "Im Schlupf" (Heidenheimer Straße 7-9, 75179 Pforzheim) - www.im-schlupf.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Rückblick* - Java Forum Stuttgart und Mitgliederversammlung iJug
    * *Weiterentwicklung der JuG*
    * *Sonstiges und Termine*

  * 06.06.2019 19:00
    "Seehaus Pforzheim" (Tiefenbronner Straße 201, 75175 Pforzheim) - https://www.seehauspforzheim.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Rückblick* - Javaland und Hauptversammlung iJug
    * *Aktivitäten JugPF* - was wollen wir in nächster Zeit angehen
    * *Termine*

  * 17.04.2019 19:00
    "Highnoon Bar" (Steinerstr. 1, 75203 Königsbach-Stein) - www.gasthaus-kanne.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Rückblick* - Javaland 2019 und Mitgliederversammlung iJug
    * *Aktivitäten JugPF* - was wollen wir in nächster Zeit angehen
    * *Termine*

  * 20.03.2019 11:45
    "JUG Cafe im Javaland" - www.javaland.eu (@gzilly)
 
  * 7.02.2019 19:00
    "Im Schlupf" (Heidenheimer Straße 7-9, 75179 Pforzheim) - www.im-schlupf.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Javaland 2019* - Stand Planung für das Javaland 2019
    * *Sonstiges und Termine*

  * 22.11.2018
    "Im Schlupf" (Heidenheimer Straße 7-9, 75179 Pforzheim) - www.im-schlupf.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *EclipseConf* - Bericht von der EclipseCon Europe 2018
    * *Javaland 2019* - Stand Planung für das Javaland 2019
    * *Sonstiges und Termine*

  * 04.10.2018
    "Im Schlupf" (Heidenheimer Straße 7-9, 75179 Pforzheim) - www.im-schlupf.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *EclipseConf* - Teilnahme EclipseCon Europe 2018 in Ludwigsburg
    * *Javaland 2019* - Planung für das Javaland 2019
    * *Sonstiges und Termine*

  * 12.07.2018 19:00
    "Highnoon Bar" (Steinerstr. 1, 75203 Königsbach-Stein) - www.gasthaus-kanne.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Rückblick* - Java Forum Stuttart und Mitgliederversammlung iJug
    * *Aktivitäten JugPF* - was wollen wir in nächster Zeit angehen
    * *Termine*

  * 03.05.2018 19:00
    "Biergarten Seehaus Pforzheim" (Tiefenbronner Straße 201, 75175 Pforzheim) (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Rückblick* - Javaland und Hauptversammlung iJug
    * *Aktivitäten JugPF* - was wollen wir in nächster Zeit angehen
    * *Termine*

  * 15.02.2018 19:00
    "Ketterers Braustüble" (Jahnstrasse 10, 75173 Pforzheim) (@ckunz)
    * *Neuigkeiten* - Allgemeine Neuigkeiten
    * *Javaland 2018* - Unsere Aktivitäten

  * 16.11.2017 19:00
    "Onkel Otto" (Durlacher Straße 57)  (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten, Java 9    * *Java 4 Kids* - Java Flipper - Review der Veranstaltung (@gzilly)
    * *Javaland 2018* - Teilnahme JugPF (@mkarg)
    * *Termine*

  * 07.10.2017  Java4Kids

  * 16.09.2017  Plugfest Fa. ProSeS - Vorbereitungstreffen für Java4Kids

  * 27.07.2017 19:00
    "Highnoon Bar" (Steinerstr. 1, 75203 Königsbach-Stein) - www.gasthaus-kanne.de (@gzilly)
    * *Neuigkeiten* - Allgemeine Neuigkeiten, Java 9
    * *Java 4 Kids* - Java Flipper -Planungn für den nächsten Termin (@gzilly)
    * *Java Forum Stuttgart* - Bericht von der Veranstaltung (@mkarg)
    * *IJug Mitgliederversammulung* - Bericht (@mkarg)
    * *Termine* - z. B. Teilnahme JavaLand 2018

  * 01.06.2017 19:00
    "Palm Beach" (Wilferdinger Str. 66) (@gzilly)
    * *Java 4 Kids* - Wie geht es mit dem Projekt weiter? (@gzilly)
    * *Termine* - z. B. weitere Stammtische, Java Forum Stuttgart (@mkarg)

  * 28.03.2017 13:00
    JUG-Cafe auf der JavaLand Konferenz in Brühl

  * 09.02.2017 19:00
    Restaurant "Akropolis" (Wurmberger Str. 45, Pforzheim) (@gzilly)
    * *Java 4 Kids* - Stand der Vorbereitungen (@gzilly); T-Shirt (@mkarg);
        Ort und Termin (@mkarg)
    * *Javaland 2017* - Was gibt es noch zu organisieren?
    * *Termine* - z. B. weitere Stammtische

  * 17.11.2016 19:00
    ESV Gaststätte "zum Eisenbahner" (Emil-Kessler-Str. 15, Pforzheim) (@gzilly)
    * *Java 4 Kids* - Stand der Vorbereitungen (@gzilly); T-Shirt (@mkarg);
        Teilnehmerzahl und Alter (@mkarg)
    * *Java EE 8* - Was gibt es neues seit der JavaOne? (@mkarg)

  * 08.09.2016 19:00 Restaurant "Ketterers Braustüble"
    (Jahnstraße, Pforzheim) (@ckunz)
    * *Java 4 Kids* - Stand der Vorbereitungen (@gzilly)
    * *iJUG-Mitgliederversammlung* (06.07.2016) - Besuchsbericht (@mkarg)
    * *Java Forum Stuttgart* (07.07.2016) - Besuchsbericht (@mkarg)

  * 30.06.2016 19:00 Restaurant "Hopfenschlingel"
    (Sedanplatz, Pforzheim) (@mkarg)
    * *Java 4 Kids* - Stand der Vorbereitungen, Ziele, Projektthema (@gzilly)
    * *Java EE Guardians* (@mkarg)
    * *Java EE Micro Profile* (@mkarg)
    * Ehrengast: *Tobias FRECH* (iJUG, JUGS, Javaland) (@mkarg)
